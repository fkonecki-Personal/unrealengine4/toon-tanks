// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Tank.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"

void ATower::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (IsTankInRange()) {
		RotateTurret(Tank->GetActorLocation());
	}
}

void ATower::BeginPlay() {
	Super::BeginPlay();

	Tank = Cast<ATank>(UGameplayStatics::GetPlayerPawn(this, 0));

	GetWorldTimerManager().SetTimer(FireRateTimerHandle, this, &ATower::CheckFireCondition, FireRate, true);
}

void ATower::HandleDestruction() {

	Super::HandleDestruction();
	Destroy();
}


void ATower::CheckFireCondition() {
	if (IsTankInRange() && Tank && Tank->bAlive) {
		Fire();
	}
}

bool ATower::IsTankInRange() {
	return Tank ? 
		FireRange >= FVector::Dist(GetActorLocation(), Tank->GetActorLocation()) : 
		false;

}

